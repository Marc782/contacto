package com.marvin.cerdas.angulo.contacto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DatosContacto extends AppCompatActivity {

    private TextView nombreCompleto;
    private TextView fechaNacimiento;
    private TextView telefono;
    private TextView email;
    private TextView descripcion;
    private Button buton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.datos_contacto);

        // Se inicializan todos los componentes del view.
        nombreCompleto = (TextView) findViewById(R.id.tv_NombreCompleto);
        fechaNacimiento = (TextView) findViewById(R.id.tv_FechaNacimiento);
        telefono  = (TextView) findViewById(R.id.tv_Telefono);
        email  = (TextView) findViewById(R.id.tv_Email);
        descripcion = (TextView) findViewById(R.id.tv_Descripcion);
        buton  = (Button) findViewById(R.id.btn_EditarDatos);

        // Se reciben todos los datos de la actividad anterior
        Bundle parametros = getIntent().getExtras();
        final String nombre = parametros.getString("nombre");
        final String fecha = parametros.getString("fecha");
        final String telef = parametros.getString("telefono");
        final String correo = parametros.getString("email");
        final String descrip = parametros.getString("descripcion");

        // Se setean los views con los patametros recibidos.
        nombreCompleto.setText("Nombre: " + nombre);
        fechaNacimiento.setText("Fecha de nacimiento: " + fecha);
        telefono.setText("Teléfono: " + telef);
        email.setText("Email: " + correo);
        descripcion.setText("Descripción: " + descrip);

        // Se configuta el boton para que rregrese a la activity principal
        buton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DatosContacto.this, MainActivity.class);
                intent.putExtra("nombre", nombre);
                intent.putExtra("fecha", fecha);
                intent.putExtra("telefono", telef);
                intent.putExtra("email", correo);
                intent.putExtra("descripcion", descrip);
                startActivity(intent);
            }
        });
    }
}
