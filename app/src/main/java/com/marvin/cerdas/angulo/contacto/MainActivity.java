package com.marvin.cerdas.angulo.contacto;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    // Inicialización de los componentes de la vista
    private EditText nombreCompleto;
    private EditText fechaNacimiento;
    private EditText telefono;
    private EditText email;
    private EditText descripcion;
    private Button boton;
    // Variables de fecha
    private static final String CERO = "0";
    private static final String BARRA = "/";

    // Calendario para obtener fecha & hora
    public final Calendar c = Calendar.getInstance();

    // Variables para obtener la fecha
    final int mes = c.get(Calendar.MONTH);
    final int dia = c.get(Calendar.DAY_OF_MONTH);
    final int anio = c.get(Calendar.YEAR);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Se definen los componentes del formulario
        nombreCompleto = (EditText) findViewById(R.id.tiet_NombreCompleto);
        fechaNacimiento = (EditText) findViewById(R.id.tiet_FechaNacimiento);
        telefono = (EditText) findViewById(R.id.tiet_Telefono);
        email = (EditText) findViewById(R.id.tiet_Email);
        descripcion = (EditText) findViewById(R.id.tiet_Descripcion);
        boton = (Button) findViewById(R.id.btn_AgregarDatos);

        // Se verifica si se estan enviando parametros de la segunda actividad
        Bundle parametros = getIntent().getExtras();
        if (parametros != null) {
            final String nombre = parametros.getString("nombre");
            final String fecha = parametros.getString("fecha");
            final String telef = parametros.getString("telefono");
            final String correo = parametros.getString("email");
            final String descrip = parametros.getString("descripcion");
            cargaValores(nombre, fecha, telef, correo, descrip);
        }

        // Se ejecuta el Picker de la fecha cuando se le da click al campo de la fecha
        fechaNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                optenerFechaNacimiento();
            }
        });

        // Se ejecuta la apertura del siguiente Activity
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkFields()){
                    String nombre = nombreCompleto.getText().toString();
                    String fecha = fechaNacimiento.getText().toString();
                    String telef = telefono.getText().toString();
                    String correo = email.getText().toString();
                    String descrip = descripcion.getText().toString();
                    openDatosConta(nombre,fecha, telef, correo, descrip);
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getText(R.string.error_formulario), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    // Este método crea el Picker de fecha
    private void optenerFechaNacimiento(){
        DatePickerDialog recogerFecha = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                final int mesActual = month + 1;
                String diaFormateado = (dayOfMonth < 10)? CERO + String.valueOf(dayOfMonth):String.valueOf(dayOfMonth);
                String mesFormateado = (mesActual < 10)? CERO + String.valueOf(mesActual):String.valueOf(mesActual);
                fechaNacimiento.setText(diaFormateado + BARRA + mesFormateado + BARRA + year);
            }
        }, anio, mes, dia);
        recogerFecha.show();
    }

    // Este método abré en segundo activity y envía los datos
    private void openDatosConta(String nombre, String fecha, String telefono, String email, String descripcion){
        Intent intent = new Intent(MainActivity.this, DatosContacto.class);
        intent.putExtra("nombre", nombre);
        intent.putExtra("fecha", fecha);
        intent.putExtra("telefono", telefono);
        intent.putExtra("email", email);
        intent.putExtra("descripcion", descripcion);
        startActivity(intent);
    }

    // Este método verifica que todos los campos estén llenos
    private boolean checkFields(){
        if(nombreCompleto.getText().toString().equals("") ||
        fechaNacimiento.getText().toString().equals("") ||
        telefono.getText().toString().equals("") ||
        email.getText().toString().equals("") ||
        descripcion.getText().toString().equals("")){
            return false;
        } else {
            return true;
        }
    }

    // Este método rellena los campos cuando la información proviene de la segunda actividad
    private void cargaValores(String nombre, String fecha, String telefo, String correo, String descrip){
        nombreCompleto.setText(nombre);
        fechaNacimiento.setText(fecha);
        telefono.setText(telefo);
        email.setText(correo);
        descripcion.setText(descrip);
    }
}
